const express = require('express'); //เรียกใช้ express ผ่าน require
const myApp = express(); //สร้างตัวแปร myApp เพื่อใช้งาน express 
const port = 3030; //พอร์ตของ Server ที่ใช้ในการเปิด Localhost 
const jetson = require("./jetson.js")
const multer = require('multer');
const Jetson = new jetson();

const path = require('path');
const fs = require("fs");

// ตั้งค่าที่จัดเก็บไฟล์
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'sample_img/'); // โฟลเดอร์ที่จะจัดเก็บไฟล์
  },
  filename: (req, file, cb) => {
    // cb(null, Date.now() + path.extname(file.originalname)); // ตั้งชื่อไฟล์ใหม่ด้วยวันที่และเวลาปัจจุบัน
    cb(null, file.originalname);
  }
});

const upload = multer({ storage: storage });
// สร้าง route สำหรับการอัปโหลดไฟล์
myApp.post('/api/v1/ai/traffic_detections', upload.single('photo'), async (req, res) => {
  console.log('File uploaded to:', req.file.path);
  var path = req.file.path
  var detection_data = await Jetson.call_detection(path)
  console.log(detection_data);
  res.status(200).json(detection_data);
});


myApp.get('/api/v1/ai/traffic_detections', async (req, res) => {
  return 'developing'
}); 


myApp.listen(port, () => {
  console.log(`Server running at <http://localhost>:${port}/`);
});