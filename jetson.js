const axios = require("axios");
const fs = require("fs");
var path = require('path');
// const cv = require('opencv4nodejs');
const gm = require('gm'); 

module.exports = class jetson {

    async call_detection(file) {

        const image = fs.readFileSync(file, {
            encoding: "base64"
        });

        const demo = await axios({
            method: "POST",
            url: "https://detect.roboflow.com/hatyai_traffic/4",
            params: {
                api_key: "kyH8T6hwRTLFZdDaFy9t"
            },
            data: image,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })
            .then(async function (response) {
                console.log("response===>", response.data);
                var data = await response.data.predictions
                var detection_data = []
                const img_w = response.data.image.width;
                const img_h = response.data.image.height;

                if (data != undefined) {
                    for (let index = 0; index < data.length; index++) {
                        const e = data[index];
                        const label = e.class
                        const width = e.width
                        const height = e.height

                        var x1 = parseFloat(e.x) - (width / 2)
                        var y1 = parseFloat(e.y) - (height / 2)
                        var x2 = parseFloat(e.x) + (width / 2)
                        var y2 = parseFloat(e.y) + (height / 2)

                        x1 = x1/img_w;
                        y1 = y1/img_h;
                        x2 = x2/img_w;
                        y2 = y2/img_h;

                        const score = e.confidence
                        const class_id = e.class_id
                        const class_name = e.class
                        var mid = ""

                        // gm(image).stroke("green", 3).drawRectangle(x1, y1, x2, y2)

                        if (class_name == "Bus") {
                            // color = (255, 0, 247)  # สีชมพู
                            mid = "b/b00" + parseInt(class_id).toString()
                        } else if (class_name == "Motorcycle") {
                            mid = "s/m00" + parseInt(class_id).toString()
                        } else if (class_name == "Car") {
                            mid = "m/c00" + parseInt(class_id).toString()
                        } else if (class_name == "Truck") {
                            mid = "b/t00" + parseInt(class_id).toString()
                        } else if (class_name == "Bicycle") {
                            mid = "s/b00" + parseInt(class_id).toString()
                        }

                        var normalizedVertices = [
                            { "x": x1, "y": y1 },
                            { "x": x2, "y": y1 },
                            { "x": x2, "y": y2 },
                            { "x": x1, "y": y2 }
                        ]

                        // console.log("normalizedVertices====>",normalizedVertices);

                        detection_data.push({
                            "name": label,
                            "mid": mid,
                            "score": score,
                            "boundingPoly": {
                                "normalizedVertices": normalizedVertices
                            }
                        })
                    }
                }
                console.log("normalizedVertices===>", normalizedVertices);
                var res = {
                    "responses": [
                        { "localizedObjectAnnotations": detection_data }]
                }
                return res


            })
            .catch(function (error) {
                console.log(error.message);
            });
        return demo

    }


}