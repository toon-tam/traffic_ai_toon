FROM node:17

WORKDIR /app

#Install app dependencies
COPY package.json ./

RUN npm install

EXPOSE 3030

CMD ["node","api.js"]